import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private token: string = '123';

  constructor(
    private http: HttpClient
  ) { }

  listarProductos(termino: string) {
    let headers = new HttpHeaders({
      'token': this.token
    });

    return this.http.get(`http://localhost:4000/productos/${termino}`, {headers: headers})

  }

}

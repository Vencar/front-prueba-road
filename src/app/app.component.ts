import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductosService } from './services/productos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  form: FormGroup;

  title = 'road-front';
  listaProductos: [] = [];

  constructor(
    private prodService: ProductosService,
    private fb: FormBuilder
  ) {
    this.crearForm();
    console.log(this.listaProductos.length);
  }

  crearForm() {
    this.form = this.fb.group({
      termino: ['']
    });
  }

  buscarProductos() {
    console.log(this.form.value['termino']);
    this.prodService.listarProductos(this.form.value['termino']).subscribe(response => {
      if(response['status']) {
        let data = response['data'];
        this.listaProductos = data;
        console.log(this.listaProductos);
      }
    })
  }

  
}
